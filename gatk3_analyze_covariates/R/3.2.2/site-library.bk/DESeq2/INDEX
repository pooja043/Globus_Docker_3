DESeq                   Differential expression analysis based on the
                        negative binomial distribution
DESeqDataSet-class      DESeqDataSet object and constructors
DESeqResults-class      DESeqResults object and constructor
coef                    Extract a matrix of model coefficients/standard
                        errors
collapseReplicates      Collapse replicates in a SummarizedExperiment
                        or DESeqDataSet
counts                  Accessors for the 'counts' slot of a
                        DESeqDataSet object.
design                  Accessors for the 'design' slot of a
                        DESeqDataSet object.
dispersionFunction      Accessors for the 'dispersionFunction' slot of
                        a DESeqDataSet object.
dispersions             Accessor functions for the dispersion estimates
                        in a DESeqDataSet object.
estimateDispersions     Estimate the dispersions for a DESeqDataSet
estimateDispersionsGeneEst
                        Low-level functions to fit dispersion estimates
estimateSizeFactors     Estimate the size factors for a DESeqDataSet
estimateSizeFactorsForMatrix
                        Low-level function to estimate size factors
                        with robust regression.
fpkm                    FPKM: fragments per kilobase per million mapped
                        fragments
fpm                     FPM: fragments per million mapped fragments
makeExampleDESeqDataSet
                        Make a simulated DESeqDataSet
nbinomLRT               Likelihood ratio test (chi-squared test) for
                        GLMs
nbinomWaldTest          Wald test for the GLM coefficients
normalizationFactors    Accessor functions for the normalization
                        factors in a DESeqDataSet object.
plotDispEsts            Plot dispersion estimates
plotMA                  MA-plot from base means and log fold changes
plotPCA                 Sample PCA plot from variance-stabilized data
replaceOutliers         Replace outliers with trimmed mean
results                 Extract results from a DESeq analysis
rlog                    Apply a 'regularized log' transformation
show                    Show method for DESeqResults objects
sizeFactors             Accessor functions for the 'sizeFactors'
                        information in a DESeqDataSet object.
varianceStabilizingTransformation
                        Apply a variance stabilizing transformation
                        (VST) to the count data
