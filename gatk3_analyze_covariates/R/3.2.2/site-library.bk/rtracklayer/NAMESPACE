useDynLib(rtracklayer, .registration = TRUE)

## ============================================================
## Import
## ============================================================

import(methods)
import(BiocGenerics)
import(zlibbioc)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
### base packages
###

importFrom("stats", offset, setNames)
importFrom("utils", count.fields, URLdecode, URLencode, browseURL, download.file,
           read.table, type.convert, write.table)
importFrom("tools", file_path_as_absolute, file_path_sans_ext)
importFrom("grDevices", col2rgb, rgb)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
### Non-bioconductor packages
###

importFrom("XML", getNodeSet, xmlValue, xmlAttrs, htmlTreeParse,
           xmlInternalTreeParse, parseURI, newXMLNode, xmlChildren,
           addChildren, removeChildren)
importMethodsFrom("XML", saveXML)

importFrom("RCurl", curlUnescape, fileUpload, getCurlHandle, getForm, getURL,
           postForm)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
### Bioconductor packages
###

import(XVector)

importClassesFrom("IRanges",
                  IRanges, RangedData, RangedDataList,
                  Vector, DataTableORNULL, characterORNULL,
                  RangedSelection, SimpleList, DataFrame, RangesList,
                  Rle, RleList)
importFrom("IRanges", CharacterList, DataFrame, IRanges, RangedDataList,
           RangedData, RangesList, RangedSelection, RleList, isSingleString,
           recycleIntegerArg, isSingleStringOrNA,
           isTRUEorFALSE, isSingleNumberOrNA, PartitioningByWidth,
           PartitioningByEnd, mseqapply)
importMethodsFrom("IRanges", elementLengths, window,
                  start, end, width, range,
                  "start<-", "end<-", "width<-",
                  split, lapply, sapply, unlist, unique,
                  as.vector, as.list, as.data.frame, as.factor,
                  colnames,  rownames,
                  "colnames<-","rownames<-",
                  ranges, values, score,
                  "ranges<-", "values<-", "score<-",
                  space, universe, metadata,
                  "universe<-", "metadata<-",
                  isDisjoint, queryHits, findOverlaps, reduce, nrow, summary,
                  resize, mcols, "mcols<-", togroup, Rle, "active<-", collapse,
                  coverage, findOverlaps, reflect, relist, runValue, shift,
                  subjectHits, subsetByOverlaps, tile, togroup)

importClassesFrom("GenomicRanges", GenomicRanges, GRanges, GRangesList,
                  GenomicRangesList, Seqinfo, GIntervalTree,
                  SimpleGenomicRangesList)
importFrom("GenomicRanges", GRanges, GRangesList, Seqinfo)
importMethodsFrom("GenomicRanges", seqnames, seqlengths, strand,  "strand<-",
                  seqinfo, "seqinfo<-", seqlevels, "seqlevels<-", "seqnames<-",
                  genome, "genome<-", merge)

importFrom("Biostrings", get_seqtype_conversion_lookup, writeXStringSet,
           DNAStringSet)
importMethodsFrom("Biostrings", masks, "masks<-", getSeq)
importClassesFrom("Biostrings", DNAStringSet, XStringSet)

importFrom("BSgenome", installed.genomes, bsapply)
importMethodsFrom("BSgenome", releaseDate, organism,
                  providerVersion, provider)
importClassesFrom("BSgenome", BSgenome)

importFrom("Rsamtools", indexTabix, bgzip, TabixFile, index)
importMethodsFrom("Rsamtools", ScanBamParam, asBam, headerTabix, isOpen, path,
                  scanTabix)
importClassesFrom("Rsamtools", RsamtoolsFile, TabixFile, BamFile)

importMethodsFrom("GenomicAlignments", readGAlignmentsFromBam, cigar)
importClassesFrom("GenomicAlignments", GAlignments)

## ============================================================
## Export
## ============================================================

exportClasses(BrowserSession, BrowserView, BrowserViewList,
              UCSCSession, UCSCView,
              UCSCData, TrackLine, BasicTrackLine, GraphTrackLine,
              Bed15TrackLine, UCSCTrackModes, BigWigSelection,
              UCSCSchema, Quickload, QuickloadGenome)

## File classes
exportClasses(RTLFile, CompressedFile, GFFFile, UCSCFile, BEDFile, WIGFile,
              ChainFile, FastaFile, GFF1File, GFF2File, GFF3File, BEDGraphFile,
              BED15File, GTFFile, GVFFile, BigWigFile, BigWigFileList, 
              TwoBitFile)

exportMethods(activeView, "activeView<-", blocks, browseGenome,
              browserSession, "browserSession<-",
              browserView, browserViews,
              close, export, export.bed, export.bed15,
              export.bedGraph, export.gff, export.gff1, export.gff2,
              export.gff3, export.ucsc, export.wig, export.bw,
              export.2bit,
              import, import.bed, import.bed15, import.bedGraph,
              import.gff, import.gff1, import.gff2, import.gff3,
              import.ucsc, import.wig, import.bw, import.chain, import.2bit,
              "sequence<-", "track<-",
              track, trackNames, "trackNames<-", getTable,
              tableNames, trackName, "trackName<-",
              tableName, "tableName<-",
              ucscTrackModes, "ucscTrackModes<-",
              ucscSchema,
              coerce, initialize,
              show, summary, "[", ucscTableQuery,
              genome, "genome<-", chrom, "chrom<-", range, "range<-",
              visible, "visible<-",
              liftOver, offset, reversed,
              nrow, formatDescription,
              referenceSequence, "referenceSequence<-",
              asBED, asGFF,
              split,
              ## from IRanges
              start, end, "start<-", "end<-",
              score, "score<-",
              as.data.frame, space, mcols,
              ## from GenomicRanges
              strand, seqinfo, "seqinfo<-",
              ## from BSgenome
              organism, releaseDate, getSeq
              )

export(genomeBrowsers, start, end, strand, "start<-", "end<-", width, "width<-",
       ranges, values, GenomicData, GenomicSelection,
       score, "score<-", as.data.frame, space,
       ucscGenomes, BigWigSelection, GRangesForUCSCGenome,
       GRangesForBSGenome, summary, seqinfo, genome, "genome<-",
       uri, Quickload, quickload, QuickloadGenome,
       organism, releaseDate, mcols, wigToBigWig,
       SeqinfoForBSGenome, SeqinfoForUCSCGenome, resource, path,
       FileForFormat)

export(GFFFile, UCSCFile, BEDFile, WIGFile,
       ChainFile, FastaFile, GFF1File, GFF2File, GFF3File, BEDGraphFile,
       BED15File, GTFFile, GVFFile, BigWigFile, BigWigFileList, TwoBitFile)
