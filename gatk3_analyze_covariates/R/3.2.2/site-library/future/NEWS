Package: future
===============

Version: 0.11.0 [2016-01-15]
o All futures now validates globals by default (globals=TRUE).
o Add multisession futures, which analogously to multicore ones,
  use multiple cores on the local machine with the difference
  that they are evaluated in separate R session running in the
  background rather than separate forked R processes.
  A multisession future is a special type of cluster futures that
  do not require explicit setup of cluster nodes.
o Add support for cluster futures, which can make use of a cluster
  of nodes created by parallel::makeCluster().
o Add futureCall(), which is for futures what do.call() is otherwise.
o Standardized how options are named, i.e. 'future.<option>'.
  If you used any future options previously, make sure to check
  they follow the above format.


Version: 0.10.0 [2015-12-30]
o Now %<=% can also assign to multi-dimensional list environments.
o Add futures(), values() and resolved().
o Add resolve() to resolve futures in lists and environments.
o Now availableCores() also acknowledges the number of CPUs
  allotted by Slurm.
o CLEANUP: Now the internal future variable created by %<=% is
  removed when the future variable is resolved.
o BUG FIX: futureOf(envir=x) did not work properly when 'x' was
  a list environment.


Version: 0.9.0 [2015-12-11]
o GLOBALS: Now globals ("unknown" variables) are identified
  using the new findGlobals(..., method="ordered") in
  globals (> 0.5.0) such that a global variable preceding
  a local variable with the same name is properly identified
  and exported/frozen.
o DOCUMENTATION: Updated vignette on common issues with the
  case where a global variable is not identified because it
  is hidden by an element assignment in the future expression.
o ROBUSTNESS: Now values of environment variables are trimmed
  before being parsed.
o ROBUSTNESS: Add reproducibility test for random number
  generation using Pierre L'Ecuyer's RNG stream regardless
  of how futures are evaluated, e.g. eager, lazy and multicore.
o BUG FIX: Errors occurring in multicore futures could prevent
  further multicore futures from being created.


Version: 0.8.2 [2015-10-14]
o BUG FIX: Globals that were copies of package objects
  were not exported to the future environments.
o BUG FIX: The future package had to be attached or
  future::future() had to be imported, if %<=% was used
  internally in another package.  Similarly, it also had
  to be attached if multicore futures where used.


Version: 0.8.1 [2015-10-05]
o eager() and multicore() gained argument 'globals', where
  globals=TRUE will validate that all global variables
  identified can be located already before the future is
  created.  This provides the means for providing the same
  tests on global variables with eager and multicore futures
  as with lazy futures.
o lazy(sum(x, ...), globals=TRUE) now properly passes `...`
  from the function from which the future is setup.  If not
  called within a function or called within a function without
  `...` arguments, an informative error message is thrown.
o Added vignette 'Futures in R: Common issues with solutions'.


Version: 0.8.0 [2015-09-06]
o plan("default") resets to the default strategy, which is
  synchronous eager evaluation unless option 'future_plan'
  or environment variable 'R_FUTURE_PLAN' has been set.
o availableCores("mc.cores") returns getOption("mc.cores") + 1L,
  because option 'mc.cores' specifies "allowed number of _additional_
  R processes" to be used in addition to the main R process.
o BUG FIX: plan(future::lazy) and similar gave errors.


Version: 0.7.0 [2015-07-13]
o ROBUSTNESS: multicore() blocks until one of the CPU cores
  is available, iff all are currently occupied by other
  multicore futures.
o multicore() gained argument 'maxCores', which makes it
  possible to use for instance plan(multicore, maxCores=4L).
o Add availableMulticore() [from (in-house) 'async' package].
o More colorful demo("mandelbrot", package="future").
o BUG FIX: old <- plan(new) now returns the old plan/strategy
  (was the newly set one).


Version: 0.6.0 [2015-06-18]
o Add multicore futures, which are futures that are resolved
  asynchronously in a separate process.  These are only
  supported on Unix-like systems, but not on Windows.


Version: 0.5.1 [2015-06-18]
o Eager and lazy futures now records the result internally
  such that the expression is only evaluated once, even if
  their errored values are requested multiple times.
o Eager futures are always created regardless of error or not.
o All Future objects are environments themselves that record
  the expression, the call environment and optional variables.


Version: 0.5.0 [2015-06-16]
o lazy() "freezes" global variables at the time when
  the future is created.  This way the result of a lazy
  future is more likely to be the same as an eager future.
  This is also how globals are likely to be handled by
  asynchronous futures.


Version: 0.4.2 [2015-06-15]
o plan() records the call.
o Added demo("mandelbrot", package="future"), which can be
  re-used by other future packages.


Version: 0.4.1 [2015-06-14]
o Added plan().
o Added eager future - useful for troubleshooting.


Version: 0.4.0 [2015-06-07]
o Distilled Future API from (in-house) 'async' package.
