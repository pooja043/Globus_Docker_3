ó
jõUc           @   s¼   d  Z  d d l m Z d d l m Z m Z d d l m Z d e f d     YZ d e f d     YZ d	 e f d
     YZ	 d e f d     YZ
 d   Z d   Z d   Z d   Z d S(   s   
Wrapper over the functions as described in Chapter 33 of the
reference manual.

Routines for approximating a data set with a non linear function

i   (   t	   _callback(   t   gsl_multifit_function_fdft   gsl_multifit_function(   t   _generic_solvert   _fsolverc           B   s   e  Z d Z e j Z e j Z e j	 Z
 e j Z e j Z e j Z e j Z e j Z e j Z d    Z d   Z d   Z d   Z d   Z d   Z RS(   c         C   s£   d  |  _ |  j d  k s t  |  j d  k s3 t  |  j d  k sH t  |  j d  k s] t  |  j d  k sr t  |  j |  j | |  |  _ d |  _	 | |  _
 d  S(   Ni    (   t   Nonet   _ptrt   _freet   AssertionErrort   _alloct   _sett   _namet   _iteratet   typet   _issett   system(   t   selfR   t   nt   p(    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   __init__   s    		c         C   s2   |  j  j   } |  j |  j | |  d |  _ d  S(   Ni   (   R   t   get_ptrR
   R   R   (   R   t   xt   f(    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   set&   s    c         C   s2   |  j  j   } |  j |  j | |  d |  _ d  S(   Ni   (   R   R   t	   _positionR   R   (   R   R   R   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   position+   s    c         C   s   |  j  |  j  S(   N(   t   _getxR   (   R   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   getx0   s    c         C   s   |  j  |  j  S(   N(   t   _getdxR   (   R   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   getdx3   s    c         C   s   |  j  |  j  S(   N(   t   _getfR   (   R   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   getf6   s    N(   t   __name__t
   __module__R   R   R    t   gsl_multifit_fsolver_allocR	   t   gsl_multifit_fsolver_freeR   t   gsl_multifit_fsolver_setR
   t   gsl_multifit_fsolver_nameR   t   gsl_multifit_fsolver_positionR   t   gsl_multifit_fsolver_iterateR   t   gsl_multifit_fsolver_getxR   t   gsl_multifit_fsolver_getdxR   t   gsl_multifit_fsolver_getfR   R   R   R   R   R   R   (    (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyR      s    														t
   _fdfsolverc           B   sq   e  Z d Z e j Z e j Z e j	 Z
 e j Z e j Z e j Z e j Z e j Z e j Z e j Z d    Z RS(   c         C   s   |  j  |  j  S(   N(   t   _getJR   (   R   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   getJH   s    N(   R    R!   R   R   R    t   gsl_multifit_fdfsolver_allocR	   t   gsl_multifit_fdfsolver_freeR   t   gsl_multifit_fdfsolver_setR
   t   gsl_multifit_fdfsolver_nameR   t   gsl_multifit_fdfsolver_positionR   t   gsl_multifit_fdfsolver_iterateR   t   gsl_multifit_fdfsolver_getxR   t   gsl_multifit_fdfsolver_getdxR   t   gsl_multifit_fdfsolver_getfR   t   gsl_multifit_fdfsolver_getJR,   R-   (    (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyR+   ;   s   										t   lmderc           B   s   e  Z d  Z e j j Z RS(   s&  
    This is an unscaled version of the LMDER algorithm.  The elements
    of the diagonal scaling matrix D are set to 1.  This algorithm may
    be useful in circumstances where the scaled version of LMDER
    converges too slowly, or the function is already scaled
    appropriately.    
    (   R    R!   t   __doc__R    t   cvart   gsl_multifit_fdfsolver_lmderR   (    (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyR8   K   s   t   lmsderc           B   s   e  Z d  Z e j j Z RS(   s{  
         This is a robust and efficient version of the Levenberg-Marquardt
     algorithm as implemented in the scaled LMDER routine in MINPACK.
     Minpack was written by Jorge J. More', Burton S. Garbow and
     Kenneth E. Hillstrom.

     The algorithm uses a generalized trust region to keep each step
     under control.  In order to be accepted a proposed new position x'
     must satisfy the condition |D (x' - x)| < \delta, where D is a
     diagonal scaling matrix and \delta is the size of the trust
     region.  The components of D are computed internally, using the
     column norms of the Jacobian to estimate the sensitivity of the
     residual to each component of x.  This improves the behavior of the
     algorithm for badly scaled functions.

     On each iteration the algorithm attempts to minimize the linear
     system |F + J p| subject to the constraint |D p| < \Delta.  The
     solution to this constrained linear system is found using the
     Levenberg-Marquardt method.

     The proposed step is now tested by evaluating the function at the
     resulting point, x'.  If the step reduces the norm of the function
     sufficiently, and follows the predicted behavior of the function
     within the trust region. then it is accepted and size of the trust
     region is increased.  If the proposed step fails to improve the
     solution, or differs significantly from the expected behavior
     within the trust region, then the size of the trust region is
     decreased and another trial step is computed.

     The algorithm also monitors the progress of the solution and
     returns an error if the changes in the solution are smaller than
     the machine precision.  The possible error codes are,

    `GSL_ETOLF'
          the decrease in the function falls below machine precision

    `GSL_ETOLX'
          the change in the position vector falls below machine

    `GSL_ETOLG'
          the norm of the gradient, relative to the norm of the
          function, falls below machine precision

     These error codes indicate that further iterations will be
     unlikely to change the solution from its current value.

    (   R    R!   R9   R    R:   t   gsl_multifit_fdfsolver_lmsderR   (    (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyR<   U   s   /c         C   s   t  j |  | | |  S(   sy  
    This function tests for the convergence of the sequence by
     comparing the last step DX with the absolute error EPSABS and
     relative error EPSREL to the current position X.  The test returns
     `GSL_SUCCESS' if the following condition is achieved,

          |dx_i| < epsabs + epsrel |x_i|

     for each component of X and returns `GSL_CONTINUE' otherwise.

    (   R    t   gsl_multifit_test_delta(   t   dxR   t   epsabst   epsrel(    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt
   test_delta   s    c         C   s   t  j |  |  S(   s  
     This function tests the residual gradient G against the absolute
     error bound EPSABS.  Mathematically, the gradient should be
     exactly zero at the minimum. The test returns `GSL_SUCCESS' if the
     following condition is achieved,

          \sum_i |g_i| < epsabs

     and returns `GSL_CONTINUE' otherwise.  This criterion is suitable
     for situations where the precise location of the minimum, x, is
     unimportant provided a value can be found where the gradient is
     small enough.

    (   R    t   gsl_multifit_test_gradient(   R?   R@   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   test_gradient   s    c         C   s   t  j |  |  S(   s­   
      This function computes the gradient G of \Phi(x) = (1/2)
     ||F(x)||^2 from the Jacobian matrix J and the function values F,
     using the formula g = J^T f.

    (   R    t   gsl_multifit_gradient(   t   JR   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   gradient¦   s    c         C   s   t  j |  |  S(   s®  
    This function uses the Jacobian matrix J to compute the covariance
    matrix of the best-fit parameters, COVAR.  The parameter EPSREL is
    used to remove linear-dependent columns when J is rank deficient.
    
    The covariance matrix is given by,
    
    covar = (J^T J)^{-1}
    
    and is computed by QR decomposition of J with column-pivoting.  Any
    columns of R which satisfy
    
          |R_{kk}| <= epsrel |R_{11}|

    are considered linearly-dependent and are excluded from the
    covariance matrix (the corresponding rows and columns of the
    covariance matrix are set to zero).

    input : J, epsrel
       J       ... Jakobian matrix
       epsrel  
    (   R    t   gsl_multifit_covar(   RF   RA   (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   covar¯   s    N(   R9   t    R    t   gsl_functionR   R   R   R   R+   R8   R<   RB   RD   RG   RI   (    (    (    sM   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/multifit_nlin.pyt   <module>	   s   ,
2				