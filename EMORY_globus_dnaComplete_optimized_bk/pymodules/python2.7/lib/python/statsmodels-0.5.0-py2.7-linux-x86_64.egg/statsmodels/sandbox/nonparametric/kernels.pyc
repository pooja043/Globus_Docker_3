ó
_âTc           @   s  d  Z  d d l Z d d l Z d d l m Z m Z m Z m Z m	 Z	 m
 Z
 d e f d     YZ d e f d     YZ d e f d	     YZ d
 e f d     YZ d e f d     YZ d e f d     YZ d e f d     YZ d e f d     YZ d e f d     YZ d S(   s  
This models contains the Kernels for Kernel smoothing.

Hopefully in the future they may be reused/extended for other kernel based
method

References:
----------

Pointwise Kernel Confidence Bounds
(smoothconf)
http://fedc.wiwi.hu-berlin.de/xplore/ebooks/html/anr/anrhtmlframe62.html
iÿÿÿÿN(   t   expt   multiplyt   squaret   dividet   subtractt   inft   NdKernelc           B   s_   e  Z d  Z d	 d	 d  Z d   Z d   Z e e e d d Z d   Z	 d   Z
 d   Z RS(
   s  Generic N-dimensial kernel

    Parameters
    ----------
    n : int
        The number of series for kernel estimates
    kernels : list
        kernels

    Can be constructed from either
    a) a list of n kernels which will be treated as
    indepent marginals on a gaussian copula (specified by H)
    or b) a single univariate kernel which will be applied radially to the
    mahalanobis distance defined by H.

    In the case of the Gaussian these are both equivalent, and the second constructiong
    is prefered.
    c         C   sm   | d  k r t   } n  | |  _ | d  k rH t j t j |   } n  | |  _ t j j | j	  |  _
 d  S(   N(   t   Nonet   Gaussiant   _kernelst   npt   matrixt   identityt   _Ht   linalgt   choleskyt   It	   _Hrootinv(   t   selft   nt   kernelst   H(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   __init__.   s    		c         C   s   |  j  S(   s   Getter for kernel bandwidth, H(   R   (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   getH:   s    c         C   s   | |  _  d S(   s   Setter for kernel bandwidth, HN(   R   (   R   t   value(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   setH>   s    t   docs   Kernel bandwidth matrixc         C   sM   t  |  } t  |  d k rB t j |  | | |  j   } | St j Sd  S(   Ni    (   t   lenR
   t   meanR   t   nan(   R   t   xst   xR   t   w(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   densityD   s
     c         C   sN   t  |  j t  rJ t j |  } | | j d  } |  j t j |   Sd S(   sA   returns the kernel weight for the independent multivariate kerneliÿÿÿÿN(   t
   isinstanceR	   t   CustomKernelR
   t   asarrayt   sum(   R   R   t   d(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   _kernweightQ   s    c         C   s   |  j  |  S(   s   
        This simply returns the value of the kernel function at x

        Does the same as weight if the function is normalised
        (   R'   (   R   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   __call__]   s    N(   t   __name__t
   __module__t   __doc__R   R   R   R   t   propertyR   R!   R'   R(   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR      s   				R#   c           B   s¤   e  Z d  Z d d d d  Z d   Z d   Z e e e d d Z d   Z	 d   Z
 d	   Z d
   Z d   Z e d    Z e d    Z d   Z d   Z RS(   sà   
    Generic 1D Kernel object.
    Can be constructed by selecting a standard named Kernel,
    or providing a lambda expression and domain.
    The domain allows some algorithms to run faster for finite domain kernels.
    g      ð?c         C   sa   | t  k r d } n  | |  _ | |  _ t |  r? | |  _ n t d   | |  _ d |  _ d S(   sÂ  
        shape should be a lambda taking and returning numeric type.

        For sanity it should always return positive or zero but this isn't
        enforced incase you want to do weird things.  Bear in mind that the
        statistical tests etc. may not be valid for non-positive kernels.

        The bandwidth of the kernel is supplied as h.

        You may specify a domain as a list of 2 values [min,max], in which case
        kernel will be treated as zero outside these values.  This will speed up
        calculation.

        You may also specify the normalisation constant for the supplied Kernel.
        If you do this number will be stored and used as the normalisation
        without calculation.  It is recommended you do this if you know the
        constant, to speed up calculation.  In particular if the shape function
        provided is already normalised you should provide
        norm = 1.0
        or
        norm = True
        g      ð?s(   shape must be a callable object/functionN(	   t   Truet
   _normconstt   domaint   callablet   _shapet	   TypeErrort   _hR   t   _L2Norm(   R   t   shapet   hR/   t   norm(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR   q   s    				c         C   s   |  j  S(   s   Getter for kernel bandwidth, h(   R3   (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   geth   s    c         C   s   | |  _  d S(   s   Setter for kernel bandwidth, hN(   R3   (   R   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   seth   s    R   s   Kernel Bandwidthc            s      f d   }   j  d k r+ | | f St | t | |   } t |  d k rq t |   \ } } | | f Sg  g  f Sd S(   sW   
        Returns the filtered (xs, ys) based on the Kernel domain centred on x
        c            s9   |  d    j  } |  j d k o8 |  j d k S(   s2   Used for filter to check if point is in the domaini    i   (   R6   R/   (   t   xyt   u(   R   R   (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt
   isInDomain¢   s    i    N(   R/   R   t   filtert   zipR   (   R   R   t   ysR   R<   t   filtered(    (   R   R   s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   inDomain   s    

c         C   s±   t  j |  } t |  } |  j | | |  d } | j d k r\ | d d  d f } n  t |  d k r¦ |  j } d | t  j |  | | |  d d } | St  j Sd S(   sU   Returns the kernel density estimate for point x based on x-values
        xs
        i    i   Nt   axis(	   R
   R$   R   RA   t   ndimR   R6   R   R   (   R   R   R   R   R6   R    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR!   ±   s    	+c         C   s¦   |  j  | | |  \ } } t |  d k r t j |  | | |  j   } t j g  t | |  D]' \ } } | |  | | |  j  ^ qc  } | | St j Sd S(   s   Returns the kernel smoothing estimate for point x based on x-values
        xs and y-values ys.
        Not expected to be called by the user.
        i    N(   RA   R   R
   R%   R6   R>   R   (   R   R   R?   R   R    t   xxt   yyt   v(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   smoothÁ   s     Fc   
      C   sì   |  j  | | |  \ } } t |  d k rá t j g  | D] } |  j | | |  ^ q:  } t t | |   } t j |  | | |  j   } t j g  t	 | |  D]' \ } } | |  | | |  j  ^ q©  }	 |	 | St j
 Sd S(   sJ   Returns the kernel smoothing estimate of the variance at point x.
        i    N(   RA   R   R
   t   arrayRG   R   R   R%   R6   R>   R   (
   R   R   R?   R   RD   t
   fittedvalst   sqresidR    t   rrRF   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt	   smoothvarÐ   s    1 Fc         C   sd  |  j  | | |  \ } } t |  d k rJt j g  | D] } |  j | | |  ^ q:  } t t | |   } t j |  | | |  j   } t j g  t	 | |  D]' \ } } | |  | | |  j  ^ q©  }	 |	 | }
 t j
 |
  } |  j } |  j | | |  } | | t j
 | |  j |  j  } | | | | | f St j t j t j f Sd S(   sL   Returns the kernel smoothing estimate with confidence 1sigma bounds
        i    N(   RA   R   R
   RH   RG   R   R   R%   R6   R>   t   sqrtt   L2Normt
   norm_constR   (   R   R   R?   R   RD   RI   RJ   R    RK   RF   t   vart   sdt   Kt   yhatt   err(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt
   smoothconfÞ   s    1 F
	%c            s     j  d k r   f d   }   j d k rP t j j | t t  d   _  q t j j |   j d   j d  d   _  n    j  S(   sA   Returns the integral of the square of the kernal from -inf to infc            s     j    j |   d S(   Ni   (   RO   R1   (   R   (   R   (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   <lambda>÷   s    i    i   N(   R4   R   R/   t   scipyt	   integratet   quadR   (   R   t   L2Func(    (   R   s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRN   ó   s    #c         C   s   |  j  d k rz |  j d k r= t j j |  j t t  } n) t j j |  j |  j d |  j d  } d | d |  _  n  |  j  S(   sM   
        Normalising constant for kernel (integral from -inf to inf)
        i    i   g      ð?N(   R.   R   R/   RW   RX   RY   R1   R   (   R   t   quadres(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRO   ÿ   s    c         C   s   |  j  |  j |  S(   s0   This returns the normalised weight at distance x(   RO   R1   (   R   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   weight  s    c         C   s   |  j  |  S(   s   
        This simply returns the value of the kernel function at x

        Does the same as weight if the function is normalised
        (   R1   (   R   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR(     s    N(   R)   R*   R+   R   R   R8   R9   R,   R6   RA   R!   RG   RL   RU   RN   RO   R\   R(   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR#   f   s   "								t   Uniformc           B   s   e  Z d  d  Z RS(   g      ð?c      
   C   s;   t  j |  d d   d | d d d g d d d |  _ d  S(	   NR5   c         S   s   d S(   Ng      à?(    (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRV     s    R6   R/   g      ð¿g      ð?R7   g      à?(   R#   R   R4   (   R   R6   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR     s    (   R)   R*   R   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR]     s   t
   Triangularc           B   s   e  Z d  d  Z RS(   g      ð?c      
   C   s?   t  j |  d d   d | d d d g d d d d	 |  _ d  S(
   NR5   c         S   s   d t  |   S(   Ni   (   t   abs(   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRV   !  s    R6   R/   g      ð¿g      ð?R7   g       @g      @(   R#   R   R4   (   R   R6   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR      s    (   R)   R*   R   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR^     s   t   Epanechnikovc           B   s   e  Z d  d  Z RS(   g      ð?c      
   C   s;   t  j |  d d   d | d d d g d d d |  _ d  S(	   NR5   c         S   s   d d |  |  S(   Ng      è?i   (    (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRV   '  s    R6   R/   g      ð¿g      ð?R7   g333333ã?(   R#   R   R4   (   R   R6   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR   &  s    (   R)   R*   R   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR`   %  s   t   Biweightc           B   s/   e  Z d  d  Z d   Z d   Z d   Z RS(   g      ð?c      
   C   s?   t  j |  d d   d | d d d g d d d d	 |  _ d  S(
   NR5   c         S   s   d d |  |  d S(   Ng      î?i   i   (    (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRV   -  s    R6   R/   g      ð¿g      ð?R7   g      @g      @(   R#   R   R4   (   R   R6   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR   ,  s    c         C   s»   |  j  | | |  \ } } t |  d k r° t j t t d t t t | |  |  j      } t j t | t t d t t t | |  |  j       } | | St j	 Sd S(   sÏ   Returns the kernel smoothing estimate for point x based on x-values
        xs and y-values ys.
        Not expected to be called by the user.

        Special implementation optimised for Biweight.
        i    i   N(
   RA   R   R
   R%   R   R   R   R6   R   R   (   R   R   R?   R   R    RF   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRG   1  s    !'c   	      C   s  |  j  | | |  \ } } t |  d k rö t j g  | D] } |  j | | |  ^ q:  } t t | |   } t j t t d t t t | |  |  j	      } t j t
 | t t d t t t | |  |  j	       } | | St j Sd S(   sS   
        Returns the kernel smoothing estimate of the variance at point x.
        i    g      ð?i   N(   RA   R   R
   RH   RG   R   R   R%   R   R6   R   R   (	   R   R   R?   R   RD   RI   t   rsR    RF   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRL   C  s    1!'c         C   sv  |  j  | | |  \ } } t |  d k r\t j g  | D] } |  j | | |  ^ q:  } t t | |   } t j t t d t t t | |  |  j	      } t j t
 | t t d t t t | |  |  j	       } | | }	 t j |	  }
 |  j } |  j | | |  } |
 | t j d | |  j	  } | | | | | f St j t j t j f Sd S(   sL   Returns the kernel smoothing estimate with confidence 1sigma bounds
        i    g      ð?i   g      î?N(   RA   R   R
   RH   RG   R   R   R%   R   R6   R   RM   RN   R   (   R   R   R?   R   RD   RI   Rb   R    RF   RP   RQ   RR   RS   RT   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRU   T  s    1!'
	"(   R)   R*   R   RG   RL   RU   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRa   +  s   		t	   Triweightc           B   s   e  Z d  d  Z RS(   g      ð?c      
   C   s?   t  j |  d d   d | d d d g d d d d	 |  _ d  S(
   NR5   c         S   s   d d |  |  d S(   Ng     ñ?i   i   (    (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRV   k  s    R6   R/   g      ð¿g      ð?R7   g     àu@g     Ðz@(   R#   R   R4   (   R   R6   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR   j  s    (   R)   R*   R   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRc   i  s   R   c           B   s#   e  Z d  Z d d  Z d   Z RS(   sN   
    Gaussian (Normal) Kernel

    K(u) = 1 / (sqrt(2*pi)) exp(-0.5 u**2)
    g      ð?c      
   C   sI   t  j |  d d   d | d d  d d d d t j t j  |  _ d  S(   NR5   c         S   s   d t  j |  d d  S(   NgQ6Ô3EÙ?i   g       @(   R
   R    (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRV   v  s   R6   R/   R7   g      ð?g       @(   R#   R   R   R
   RM   t   piR4   (   R   R6   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR   u  s    c      
   C   s   t  j t t t t t | |  |  j   d    } t  j t | t t t t t | |  |  j   d     } | | S(   sÏ   Returns the kernel smoothing estimate for point x based on x-values
        xs and y-values ys.
        Not expected to be called by the user.

        Special implementation optimised for Gaussian.
        g      à¿(   R
   R%   R    R   R   R   R   R6   (   R   R   R?   R   R    RF   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRG   z  s
    $(   R)   R*   R+   R   RG   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR   o  s   t   Cosinec           B   s   e  Z d  Z d d  Z RS(   sO   
    Cosine Kernel

    K(u) = pi/4 cos(0.5 * pi * u) between -1.0 and 1.0
    g      ð?c      
   C   sF   t  j |  d d   d | d d d g d d t j d d	 |  _ d  S(
   NR5   c         S   s   d t  j t  j d |   S(   Ng-DTû!é?g       @(   R
   t   cosRd   (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRV     s   R6   R/   g      ð¿g      ð?R7   i   g      0@(   R#   R   R
   Rd   R4   (   R   R6   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyR     s    (   R)   R*   R+   R   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyRe     s   (   R+   t   numpyR
   t   scipy.integrateRW   R    R   R   R   R   R   t   objectR   R#   R]   R^   R`   Ra   Rc   R   Re   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/sandbox/nonparametric/kernels.pyt   <module>   s   .K³>